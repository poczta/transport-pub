<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="style2.css" />

  <?php
  include "sys-config/lang.php";
  include "sys-config/config.php";
  include "sys-backend/nologin.php";

  echo '<title>' . $lang_attach . '</title>
</head>
<body>
  <div id="header">
    <div id="logo">
      <h3>' . $lang_attach . '</h3>
    </div>
  </div>
  <center>
    <div id="wrapper">
      <div id="content">';
  session_start();
  if ($_SESSION['user_id'] != null) {
    $zalogowanyID = $_SESSION['user_id'];
    $today = date("Y-m-d H:i:s");
    $s1 = strtotime($today);
    $datetoday =  date('m/d/Y', $s1);
    $getFreightID = $_GET['id'];
    $geterr = $_GET['err'];

    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    echo '
          <table style="width:80%">
          <form action="sys-backend/useruploadpro.php" method="post" enctype="multipart/form-data">';

    echo ' <tr><td><label>' . $lang_type . '</label></td><td>
    <select name="type">
    <option value=11>CMR</option>
    <option value=12>Palettenschein</option>
    <option value=13>Delivery Note</option>
    <option value=14>Speditionsauftrag</option>
    <option value=15>Frachtbrief</option>
    <option value=16>Lieferschein</option>
    <option value=17>' . $lang_other . '</option>
  </select></td></tr><tr></tr>';

    echo '<tr><td colspan="2"><input type="file" name="image" accept="image/png, image/jpeg"></input></td></tr>';
    echo '</td></tr><tr><td colspan="2"><input type=hidden name="cost" value=0><input type=hidden name="status" value=3></input><input type=hidden name="freightid" value=' . $getFreightID . '></input>
  <div class="form-group" style="text-align: center; float:right">

  <input type="submit" value="&nbsp;' . $lang_add_attach . '&nbsp;" id=btnSubmit>
  </div>


	</form></td></tr></table>';


  $geterr = $_GET['err'];
  if ($geterr == 2) {
    echo "<font color='green'><h2>$lang_attch_OK</font></h2>";
  } elseif ($geterr == 1) {
    echo "<font color='red'><h2>$lang_attch_Err</font></h2>";
  }


  $sql = "SELECT * FROM `image` WHERE `userid` = $zalogowanyID and `freightid` = $getFreightID ORDER BY sysid DESC limit 20";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {

    echo ' <br>';
    echo "<div id='table'><table cellspacing='0' cellpadding='10' >  
 <th align='center'>$lang_date</th>
 <th align='center'>$lang_type</th>
 <th align='center'>$lang_attach</th>
 <th align='center'>$lang_action</th>";

    echo "</tr>";
    while ($r = $result->fetch_assoc()) {
      $getsysid = $r['sysid'];
      $getFilename = $r['Filename'];
      $gettype = $r['type'];
      $getusername = $r['username'];
      $getdate = $r['timestamp'];
      $getuserid = $r['userid'];
      $getcost = $r['cost'];
      $getstatus = $r['status'];

      $s2 = strtotime($getdate);
      $dateview =  date('d-m-Y', $s2);

      if ($getstatus == 0) {
        $statusView = "$lang_unPaid";
      } else {
        $statusView = "$lang_paid";
      }
      switch ($gettype) {
        case 11:
          $typeView = "CMR";
          break;
        case 12:
          $typeView = "Palettenschein";
          break;
        case 13:
          $typeView =  "Delivery Note";
          break;
        case 14:
          $typeView = "Speditionsauftrag";
          break;
        case 15:
          $typeView = "Frachtbrief";
          break;
        case 16:
          $typeView = "Lieferschein";
          break;
        case 17:
          $typeView = "$lang_other";
          break;
      }



      echo '<tr>';
      echo '
                  <td align="center">' . $dateview . '</td>
                  <td align="center">' . $typeView . '</td>
                  <td align="center"><a href=AttachView.php?id=' . $getFilename . '&back=' . $getFreightID . '>' . $lang_preview . '</a></td>
                  <td align="center"><a href=sys-backend/deleteattachments.php?id=' . $getsysid . '&fre=' . $getFreightID . '><img src="sys-config/trash.png" style="width:30px;height:30px;"></a>  </td>';
    }
    echo "   </tr><tr><td colspan = 4> <form action='work.php'><input type='submit' value='&nbsp;$lang_finish_adding &nbsp;' style='background-color:green;color:white; border-radius: 8px; font-size: 17px'></input></table></div><br> ";
  }
  $conn->close();
  echo "
  </div>
  </div>
  <div id='menu'><a href='index.php'>$lang_main</a>";
} else {

  echo $nologin;
}



  ?>
  <script>
  document.getElementById('btnSubmit').onclick = function(){
 this.style.backgroundColor = "red";}
</script>

  </div>
  </div>
  </body>

</html>