<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style2.css" />

    <?php
    include "sys-config/lang.php";
    include "sys-config/config.php";
    include "sys-backend/nologin.php";

    echo '
    <title>' . $lang_holidays . '</title>
</head>

<body>
    <div id="header">
        <div id="logo">
           
            <h3>' . $lang_holidays . '</h3>
        </div>
    </div>

    <div id="wrapper">
        <div id="content">';



    session_start();
    if ($_SESSION['user_id'] != null) {
        $zalogowanyID = $_SESSION['user_id'];


        $currentact = $_GET['act'];
        if ($currentact == null) {
            $currentact = date("Y-m");
        }

        $currentactdate = strtotime($currentact);
        $nextact = date("Y-m", strtotime("+1 month",  $currentactdate));
        $previosact = date("Y-m", strtotime("-1 month",  $currentactdate));

        echo ' <center>
        <button><a href="calendar.php?act=' . $previosact . '">' . $lang_previousMonth . '</a></button>
        <button><a href="calendar.php?act=' . $nextact . '">' . $lang_nextMonth . '</a></button>
       ';

        $FirstDayOfCurrentMonth = strtotime(date($currentact));
        $FirstDayOfNextMonth =  strtotime("+1 month", $FirstDayOfCurrentMonth);
        $weekday = date('l', $FirstDayOfCurrentMonth);

        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $subyear = substr($currentact, 0, -3);
        $submonth = substr($currentact, -2);

        echo "<br><br><h3>$lang_month $currentact  </h3>";
        echo "<Table border=1 style='width:70%' ><tr>
<th align='center'> $lang_monday </th>
<th align='center'> $lang_tuesday </th>
<th align='center'> $lang_wednesday </th>
<th align='center'> $lang_thursday </th>
<th align='center'> $lang_friday </th>
<th align='center'> $lang_saturday </th>
<th align='center'> $lang_sunday </th></tr><tr><td align='center' >";


        switch ($weekday) {
            case "Tuesday":
                $tab = "</td><td align='center'>";
                break;
            case "Wednesday":
                $tab  = "</td><td></td><td align='center'>";
                break;
            case "Thursday":
                $tab =  "</td><td></td><td></td><td align='center'>";
                break;
            case "Friday":
                $tab = "</td><td></td><td></td><td></td><td align='center'>";
                break;
            case "Saturday":
                $tab = "</td><td></td><td></td><td></td><td></td><td align='center'>";
                break;
            case "Sunday":
                $tab = "</td><td></td><td></td><td></td><td></td><td></td><td align='center'>";
                break;
        }

        echo $tab;


        $sql = "SELECT * FROM calendar where year=$subyear and month=$submonth ORDER BY dateblock ASC";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($r = $result->fetch_assoc()) {
                $eventid = $r['eventid'];
                $dateblock = $r['dateblock'];
                $dateview  = date('d', strtotime($dateblock));
                $blockflag = $r['blockflag'];
                $blockuser = $r['blockuser'];
                $blockuserid = $r['blockuserid'];
                $idcalendar = $r['idcalendar'];
                $weekday  = date('l', strtotime($dateblock));


                if ($blockflag == 1 && $blockuserid != $zalogowanyID) {
                    if ($weekday == "Sunday" || $weekday == "Saturday") {
                        echo "<font color='red'>";
                    }

                    echo '<h2>' . $dateview . '</h2></font>';
                    echo "<font color='red'>$lang_occupied  </font>";
                } elseif ($blockflag == 1 && $blockuserid == $zalogowanyID) {
                    if ($weekday == "Sunday" || $weekday == "Saturday") {
                        echo "<font color='red'>";
                    }

                    echo '<h2><a href="sys-backend/delmyevent.php?id=' . $idcalendar . '&eventid=' . $eventid . '"style="color:green">' . $dateview . '</a></h2>';
                    echo "<font color='black'>$lang_Delete </font>";
                } else {

                    if ($weekday == "Sunday" || $weekday == "Saturday") {
                        echo "<font color='red'>";
                        echo '<h2>' . $dateview . '</h2>';
                        echo " --- </font>";
                    } else {
                        echo '<h2><a href="sys-backend/addmyevent.php?id=' . $idcalendar . '&evendate=' . $dateblock . '"style="color:black">' . $dateview . '</a></h2>';
                        echo "$lang_add</font>";
                    }
                }

                if ($weekday == "Sunday") {
                    echo "</td></tr></tr><td align='center'>";
                } else {
                    echo "</td><td align='center'>";
                }
            }
        }

        switch ($weekday) {
            case "Monday":
                $tab = "</td><td></td><td></td><td></td><td></td><td>";
                break;
            case "Tuesday":
                $tab  = "</td><td></td><td></td><td></td><td>";
                break;
            case "Wednesday":
                $tab =  "</td><td></td><td></td><td>";
                break;
            case "Thursday":
                $tab = "</td><td></td><td>";
                break;
            case "Friday":
                $tab = "</td><td>";
                break;
            case "Saturday":
                $tab = "";
                break;
        }

        echo $tab;
        echo "</td></tr></table><br><br>";

        $conn->close();
        echo '
        </div>
        </div>
        <div id="menu">

    <ul>
        <li><a href="events.php"> ' . $lang_holidays . '</a></li>
        <li><a href="index.php?act=default">' . $lang_main . '</a></li></ul>';
    } else {
        echo $nologin;
    }


    ?>
    </center>

    </div>
    </div>

    </body>

</html>