<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style2.css" />

    <?php
    include "sys-config/lang.php";
    include "sys-config/config.php";
    include "sys-backend/dailycheck.php";
    include "sys-backend/nologin.php";
    
    echo '
    <title>' . $lang_daily . '</title>
</head>

<body>
    <div id="header">
        <div id="logo">
            <h3>' . $lang_daily . '</h3>
        </div>
    </div>
<center>
    <div id="wrapper">
        <div id="content">';

    session_start();
    if ($_SESSION['user_id'] != null) {
        $zalogowanyID = $_SESSION['user_id'];
        $today = date("Y-m-d H:i:s");
        $datetoday =  date("Y-m-d");
        $locale = $_SESSION['lang'];
        $rqc = $_GET['rqc'];

        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        echo "<h2><font color= 'black'>$lang_ToBeconfirmed</h2></font> ";

        $sql = "SELECT * FROM DailyCheckDim where `status` = 1 and `locale`= '$locale' ORDER BY sysid ASC";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {

            echo "<div id='table'><table cellspacing='0' cellpadding='10' style='table-layout:fixed' >
     <tr align='center'>
     <th colspan='1'>$lang_breakdown</th>
     <th>$lang_name </th>
     <th colspan='1'>$lang_confirm</th>
     
     </tr>";
            while ($r = $result->fetch_assoc()) {
                $name = $r['name'];
                $sys = $r['sys'];
                $idcheckv = $r['sysid'];


                // tutaj spr czy juz to zrobione 

                $sql1 = "SELECT * FROM DailyCheckFact WHERE `userid` = '$zalogowanyID' and `daily-id` = '$sys' and `date` = '$datetoday' ";
                $result1 = $conn->query($sql1);
                if ($result1->num_rows > 0) {
                    while ($r1 = $result1->fetch_assoc()) {

                        $sprdaily = $r1['daily-id'];
                    }
                } else {
                    $sprdaily = '';
                }

                if ($sprdaily == null) {
                    echo '<tr> ';
                    echo '<td align="center"><a href="sys-backend/userdailycheck.php?id=' . $sys . '&namecheck=' . $name . '&status=1"><img src="sys-config/NO.png" width="40" height="40"></a></td>';
                    echo '<td align="center">' . $name ;
                    echo '<td align="center"><a href="sys-backend/userdailycheck.php?id=' . $sys . '&namecheck=' . $name . '&status=0"><img src="sys-config/OK.png" width="40" height="40"></a></td>';
                    echo '<tr> ';
                }
                echo '</td></tr>';
            }
            echo "  </table></div><br> ";
        } else {
            echo "<font color= 'black'>Brak daily checks</font> ";
        }

        //--------------- additional repair reqests 
        echo "<table> ";
        echo "<h2><font color= 'black'>$lang_Aditiona_Repair_Request </h2></font> ";
        echo '<form action="sys-backend/AditionalRepair.php" method="POST">';
        echo '<tr><td>' . $lang_RepairName . ' </td><td> <textarea id="name" name="name" cols="20" rows="3""></textarea></td></tr>';
        if ($rqc == 1) {
            echo "<tr><td colspan=2>";
            echo $lang_repair_request_txt;
            echo "</td></tr>";
        }

        echo '<td colspan="2"><div class="form-group" style="text-align: center; float:right"><input type="submit" value="' . $lang_submit . '"></form></div></td></tr>';

        echo "</table> ";

        // nowa tablea 
        echo "<h2><font color= 'black'>$Last_5_entries </h2></font> ";

        $sql1 = "SELECT * FROM DailyCheckFact WHERE `userid`=$zalogowanyID group by `date`,`userid` ORDER BY timestamp DESC LIMIT 5";
        $result1 = $conn->query($sql1);
        if ($result1->num_rows > 0) {

            echo "<div id='table'><table cellspacing='0' cellpadding='10' >
    <tr >
    <th align='center'>$lang_date       
    <th align='center'>OK
    <th align='center'>$lang_breakdown
    <th align='center'>$lang_verified
    <th align='center'>$lang_action

    </tr>";
            while ($r = $result1->fetch_assoc()) {

                $sysidDaily = $r['sys-id'];
                $dailyName = $r['daily-name'];
                $dailyNameUA = $r['daily-nameua'];
                $olicheck = $r['olicheck'];
                $timestamp = $r['timestamp'];
                $usernamecheck = $r['username'];
                $registration = $r['registration'];
                $status = $r['status'];
                $checkuserid = $r['userid'];

                $datefoecount = date("Y-m-d", strtotime($timestamp));


                $sql2 = "SELECT COUNT(DISTINCT `daily-id`) FROM DailyCheckFact WHERE `userid`=$zalogowanyID and `date`='$datefoecount' and `status`=0 ";
                $result2 = $conn->query($sql2);
                $row2 = $result2->fetch_row();
                $checkcountOK =  $row2[0];

                $sql3 = "SELECT COUNT(DISTINCT `daily-id`) FROM DailyCheckFact WHERE `userid`=$zalogowanyID and `date`='$datefoecount' and `status`=1 ";
                $result3 = $conn->query($sql3);
                $row3 = $result3->fetch_row();
                $checkcountErr =  $row3[0];

                $sql4 = "SELECT COUNT(DISTINCT `daily-id`) FROM DailyCheckFact WHERE `userid`=$zalogowanyID and `date`='$datefoecount' and (`status`=2 or `status`=3) ";
                $result4 = $conn->query($sql4);
                $row4 = $result4->fetch_row();
                $checkcountOther =  $row4[0];


                if ($checkcountErr > 0) {
                    $color = "#ff0000";
                } else {
                    $color = "#000040";
                }

                echo '<tr>  

        <td align="center">' . $datefoecount . '</td>
        <td align="center">' . $checkcountOK . '</td>   
        <td align="center" style="color: ' . $color . '" >' . $checkcountErr . '</td>   
        <td align="center">' . $checkcountOther . '</td>  
        <td align="center"><a href="UserDaillyChecksDetail.php?id=' . $checkuserid . '&date=' . $datefoecount . '"> --==>  </a></td> ';

                echo '</td></tr>';
            }
            echo "  </table></div><br> ";
        }



        $conn->close();
        echo "
        </div>
        </div>
        <div id='menu'><a href='index.php'>$lang_main</a>";

        
    } else {

        echo $nologin;
    }

    ?>
    </div>
    </body>

</html>