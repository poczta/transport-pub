<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="style2.css" />

  <?php
  include "sys-config/lang.php";
  include "sys-config/config.php";
  include "sys-backend/nologin.php";
  

  echo '<title>' . $lang_costs . '</title>
</head>
<body>
  <div id="header">
    <div id="logo">
      <h3>' . $lang_costs . '</h3>
    </div>
  </div>
  <center>
    <div id="wrapper">
      <div id="content">';
  session_start();
  if ($_SESSION['user_id'] != null) {
    $zalogowanyID = $_SESSION['user_id'];
    $today = date("Y-m-d H:i:s");
    $s1 = strtotime($today);
    $datetoday =  date('m/d/Y', $s1);
    $geterr = $_GET['err'];

    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    echo '
          <table style="width:80%">
          <form action="sys-backend/useruploadpro.php" method="post" enctype="multipart/form-data">';

    echo ' <tr><td><label>' . $lang_type . '</label></td><td>
    <select name="type">
    <option value=1>' . $lang_vignette . '</option>
    <option value=2>' . $lang_highway . '</option>
    <option value=3>' . $lang_hotel . '</option>
    <option value=4>' . $lang_post . '</option>
    <option value=5>' . $lang_CarWash . '</option>
    <option value=6>' . $lang_parking . '</option>
    <option value=8>' . $lang_mandate . '</option>
    <option value=9>' . $lang_fuel_cash . '</option>
    <option value=7>' . $lang_other . '</option>
  </select></td></tr><tr></tr>';

    echo '<tr><td><label>' . $lang_amount . '</label></td><td><input type=number step="0.01" max = "1000" min = "1" name="cost"/></input>&nbsp;&nbsp;';
    echo ' 
    <select name="currency">
    <option value=EUR>EUR</option>
    <option value=PLN>PLN</option>
    <option value=CHF>CHF</option>
    <option value=NOK>NOK</option>
    <option value=INN>-</option>
  </select></td></tr><tr></tr>';

    echo '<tr><td colspan="2"><input type="file" name="image" accept="image/png, image/jpeg"></input></td></tr>';
    echo '</td></tr><tr><td colspan="2">

  <div class="form-group" style="text-align: center; float:right">

  <input type="submit" value="' . $lang_add_attach . '" id=btnSubmit>
  <img id="imageloading" src="sys-config/loading.gif" style="display:none;width:30px;height:30px;" />


  </div>




	</form></td></tr></table>';
 
  $geterr = $_GET['err'];
  if ($geterr == 2) {
    echo "<font color='green'><h2>$lang_attch_OK</h2></font>";
  } elseif ($geterr == 1) {
    echo "<font color='red'><h2>$lang_attch_Err</h2></font>";
  }

  $sql = "SELECT * FROM `image` WHERE `userid` = $zalogowanyID and type <=10  ORDER BY sysid DESC limit 5";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {

    echo ' <h3><br>' . $Last_5_entries . '</h3>';
    echo "<div id='table'><table cellspacing='0' cellpadding='10' >  
 <th align='center'>$lang_date</th>
 <th align='center'>$lang_type</th>
 <th align='center'>$lang_costs</th>
 <th align='center'>$lang_status</th>
 <th align='center'>$lang_attach</th>";

    echo "</tr>";
    while ($r = $result->fetch_assoc()) {
      $getsysid = $r['sysid'];
      $getFilename = $r['Filename'];
      $gettype = $r['type'];
      $getusername = $r['username'];
      $getdate = $r['timestamp'];
      $getuserid = $r['userid'];
      $getcost = $r['cost'];
      $getstatus = $r['status'];

      $s2 = strtotime($getdate);
      $dateview =  date('d-m-Y', $s2);

      if ($getstatus == 0) {
        $statusView = '<img src="sys-config/wait.png" style="width:30px;height:30px;">';
      } else {
        $statusView = '<img src="sys-config/paid.png" style="width:50px;height:30px;">';
      }



      switch ($gettype) {
        case 1:
          $typeView = "$lang_vignette";
          break;
        case 2:
          $typeView = "$lang_highway";
          break;
        case 3:
          $typeView =  "$lang_hotel";
          break;
        case 4:
          $typeView = "$lang_post";
          break;
        case 5:
          $typeView = "$lang_CarWash";
          break;
        case 6:
          $typeView = "$lang_parking";
          break;
        case 7:
          $typeView = "$lang_other";
          break;
        case 8:
          $typeView = "$lang_mandate";
          break;
        case 9:
          $typeView = "$lang_fuel_cash";
          break;
      }
      echo '<tr>';
      echo '
                  <td align="center">' . $dateview . '</td>
                  <td align="center">' . $typeView . '</td>
                  <td align="center">' . $getcost . '</td>
                  <td align="center">' . $statusView . '</td>
                  <td align="center"><a href=AttachView.php?id=' . $getFilename . '&back=0>' . $lang_preview . '</a></td>';
    }
    echo "  </table></div><br> ";
  }
  $conn->close();
  echo "
  </div>
  </div>
  <div id='menu'><a href='index.php'>$lang_main</a>";

  
} else {

  echo $nologin;
}

?>

  <script>
    document.getElementById('btnSubmit').onclick = function() {
      this.style.backgroundColor = "red";
      document.getElementById('imageloading').style.display='block';
    }

     </script>





  </div>
  </div>
  </body>

</html>