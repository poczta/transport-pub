<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style2.css" />

    <?php

    include "sys-config/lang.php";
    include "sys-config/config.php";
    include "sys-backend/workcheck.php";
    include "sys-backend/nologin.php";


    echo '
    <title>' . $lang_delegations . '</title>
</head>

<body>
    <div id="header">
        <div id="logo">
            <h3>' . $lang_delegations . '</h3>
        </div>
    </div>
    <center>
        <div id="wrapper">
            <div id="content">';

    session_start();
    if ($_SESSION['user_id'] != null) {
        $zalogowanyID = $_SESSION['user_id'];
        $today = date("Y-m-d H:i:s");
        $s1 = strtotime($today);
        $datetoday =  date('m/d/Y', $s1);

        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        //------ tu weryfikacja stanu licznika czy nie jest mniejszy lub wiekszy niz 1000km 
        $sql2 = "SELECT * FROM devices where assignedid = $zalogowanyID ORDER BY id DESC LIMIT 1";
        $result2 = $conn->query($sql2);
        if ($result2->num_rows > 0) {
            while ($r2 = $result2->fetch_assoc()) {
                $checkkmmin = $r2['CurrentRange'];
                $checkkmmax = $checkkmmin + 1000;
                $checkkmmin = $checkkmmin;
            }
        }


        if ($lastworktype == 1) {
            $disableoption = "disabled";
        } else {
            $disableoption = "";
        }
        if ($lastworktype == 1) {
            $disabletxt = $lang_delegationDisable;
        } else {
            $disabletxt = "";
        }
        if ($lastworktype == 1) {
            $disablebutton = "reset";
        } else {
            $disablebutton = "submit";
        }

        echo '<table style="width:80%">';
        echo $disabletxt;
        echo '<form action="sys-backend/adddelegation.php" method="POST">';

        echo '<tr><td><label>' . $lang_type . ' </label> </td><td>
        <select name="type" ' . $disableoption . '>
        <option value="3">' . $lang_StartDelegation . '</option>
        <option value="2">' . $lang_EndDelegation . '</option>
        </select> </td></tr><tr></tr>';

        echo '<tr><td><label>' . $lang_odometer . ' </label></td><td><input type=number name="km" min="' . $checkkmmin . '" max="' . $checkkmmax . ' " size="15" value = ' . $checkkmmin . '></input></td></tr><tr></tr>';

        $sql1 = "SELECT * FROM devices where assignedid = '$zalogowanyID' LIMIT 1";
        $result1 = $conn->query($sql1);
        if ($result1->num_rows > 0) {
            while ($r1 = $result1->fetch_assoc()) {
                $devicename = $r1['name'];
                echo '<tr><td><label>' . $lang_registration . ' </label></td><td><input type=text name="registration" value=' . $devicename . ' readonly></input></td></tr>';
            }
        } else {
            $disablebutton = "reset";
            echo "<tr><td></td><td><font color='red'><b> $lang_no_assigned_asset </b></font> </td></tr>";
        }


        echo '
        </td></tr><tr><td colspan="2">
        <div class="form-group" style="text-align: center; float:right">
        <input type="' . $disablebutton . '" value="' . $lang_submit . '">
        </div>
          </form></td></tr></table>';

        $sql = "SELECT * FROM DelegationFact where userid = $zalogowanyID ORDER BY sysid DESC limit 5";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            echo "<br><div id='table'> <h3>$Last_5_entries</h3>
                    <table cellspacing='0' cellpadding='10' >
                        <tr align='center'>
                        <th>$lang_date </th>
                        <th>Km</th>
                        <th colspan = 2>$lang_type</th>
                       
                    </tr>";

            while ($r = $result->fetch_assoc()) {
                $sysid = $r['sysid'];
                $date = $r['date'];
                $km = $r['km'];
                $s1 = strtotime($date);
                $shortdate =  date('m/d/Y', $s1);

                $type = $r['type'];

                switch ($type) {
                    case 3:
                        $typeView = $lang_StartDelegation;
                        break;
                    case 2:
                        $typeView = $lang_EndDelegation;
                        break;
                    }

                        switch ($type) {
                            case 3:
                                $typeViewimg = '<img src="sys-config/wheel.png" style="width:40px;height:40px;">';
                                break;
                            case 2:
                                $typeViewimg = '<img src="sys-config/home.png" style="width:40px;height:40px;">';
                                break;
                }


                echo '<tr><td align="center">' . $date . '</td>
                            <td align="center">' . $km . '</td>
                            <td align="center">' . $typeView . '</td>
                            <td align="center">' . $typeViewimg . '</td>';

                                // disabled deletion delegation 
      //          if (1 == 2) {
      //              echo '<td align="center"><a href="sys-backend/delmydelegation.php?id=' . $sysid . '"><img src="sys-config/trash.png" style="width:30px;height:30px;"></a></td>';
      //          } else {
      //              echo '<td align="center"><img src="sys-config/notallowed.png" style="width:40px;height:30px;"></td>';
      //          }
                echo '</td></tr>';
            }
            echo "  </table></div><br>  ";
        } else {
            echo "<font color= 'black'>No Data</font> ";
        }

        $conn->close();
        echo "
        </div>
        </div>
        <div id='menu'><a href='index.php'>$lang_main</a>";

        
    } else {

        echo $nologin;
    }

    ?>
    </div>
    </body>

</html>