    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style2.css" />

    <?php
    include "sys-config/lang.php";
    include "sys-config/config.php";
    include "sys-backend/nologin.php";

    echo '<title>' . $lang_refueling . '</title>
</head>
<body>
    <div id="header">
        <div id="logo">
            <h3>' . $lang_refueling . '</h3>
        </div>
    </div>
    <center>
    <div id="wrapper">
        <div id="content">
        <table style="width:80%">';

    session_start();
    if ($_SESSION['user_id'] != null) {


        $zalogowanyID = $_SESSION['user_id'];
        $today = date("Y-m-d H:i:s");
        $s1 = strtotime($today);
        $datetoday =  date('d-m-Y', $s1);

        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }


        $sq3 = "SELECT * FROM users where user_id=$zalogowanyID ";
        $result3 = $conn->query($sq3);
        while ($r3 = $result3->fetch_assoc()) {
            $checkadmin = $r3['CzyToAdmin'];
            $delegate = $r3['delegate'];
        }
        $_SESSION['admin'] =   $checkadmin;




        //-------------------- form start -------------------------------

        echo '<form action="sys-backend/addgasoil.php" method="POST">';
        echo '<tr><td><label for="type">' . $lang_type . '</label></td>';

        echo '<td><select name="type"  id="type">
                        <option value="0">' . $lang_fuel . '</option>
                        <option value="1">' . $lang_AdBlue . '</option>
                    </select></td></tr><tr></tr>';

        if ($_SESSION['admin'] == 1) {
            echo '<td><label>' . $lang_registration . ' </label></td><td>
            <select name="registration"';
            echo "<option value=''></option>";

            $sql1 = "SELECT * FROM devices where `status` = 'Aktywny'";
            $result1 = $conn->query($sql1);
            if ($result1->num_rows > 0) {
                while ($r1 = $result1->fetch_assoc()) {
                    $devicename = $r1['name'];
                    echo ' <option value=' . $devicename . '>' . $devicename  . '</option> ';
                }
            }
            echo '</select></td></tr><tr></tr>';
        } else {
            $sql1 = "SELECT * FROM devices where assignedid = '$zalogowanyID' LIMIT 1";
            $result1 = $conn->query($sql1);
            if ($result1->num_rows > 0) {
                while ($r1 = $result1->fetch_assoc()) {
                    $devicename = $r1['name'];
                    echo '<td><label>' . $lang_registration . ' </label></td><td><input type=text name="registration" value=' . $devicename . ' readonly></input></td></tr><tr></tr>';
                }
            }
        }
        //------ tu weryfikacja stanu licznika czy nie jest mniejszy lub wiekszy niz 1000km 
        $sql2 = "SELECT * FROM FuelFact where registration = '$devicename' ORDER BY sysid DESC LIMIT 1";
        $result2 = $conn->query($sql2);
        if ($result2->num_rows > 0) {
            while ($r2 = $result2->fetch_assoc()) {
                $checkkmmin = $r2['km'];
                $checkkmmax = $checkkmmin + 1000;
                if ($zalogowanyID == 1 || $zalogowanyID == 12) {
                    $checkkmmin = '';
                    $checkkmmax = '';
                }
            }
        }
        echo '<td><label for="km">' . $lang_odometer . ' </label></td><td><input type=number name="km" id="km" min="' . $checkkmmin . '" max="' . $checkkmmax . '"></input></tr><tr></tr>';
        echo '<td><label for="volume">' . $lang_NumberLiters . ' </label></td><td><input type=number name="volume" id="volume" max="120" ></input></tr><tr></tr>';
        echo '<td><label for="country">' . $lang_country . ' </label></td><td><input type=text name="country"  id="country" ></input></tr><tr></tr>';
        echo '<td><label for="location">' . $lang_location . ' </label></td><td> </input> <input type=text name="location" id="location" disabled></input></tr><tr></tr>';

        echo '<input type=hidden name="latitude"  id="latitude" value=""></input>';
        echo '<input type=hidden name="longitude"  id="longitude"value=""></input>';
        echo '</td></tr><tr><td >';
        echo '<button  type="button" id = "find-me" style="background-color:green;color:white; border-radius: 8px; font-size: 18px">&nbsp;' . $lang_get_locations . '&nbsp;</button><br/><p id = "geostatus">';
        echo '</td><td>';


        //($delegate != 3)
        //(1!=1) - temporary disabled
        if (1 != 1) {
            echo '<div class="form-group" style="text-align: center; float:right">';
            echo "<font color ='red'>Brak aktywnej delegacji</font>";
        } else {
            echo '<div class="form-group" style="text-align: center; float:right">';
            echo '<input type="submit" value="' . $lang_submit . '"></div>';
        }


        echo '</form></td></tr></table>';


        //------------------- end inputs and start table -------------------

        $sql = "SELECT * FROM FuelFact where userid = $zalogowanyID ORDER BY sysid DESC limit 5";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $rowcount = $result->num_rows;
            echo " <div id='table'><br><h3>$Last_5_entries</h3>
                    <table cellspacing='0' cellpadding='10' >
                        <tr align='center'>

                        <th>$lang_date</th>
                        <th>$lang_odometer</th>
                        <th>$lang_NumberLiters</th>
                        <th>$lang_consumption</th>
                        <th>$lang_action</th>
                        </tr>";

            while ($r = $result->fetch_assoc()) {
                $sysid = $r['sysid'];
                $date = $r['date'];
                $s = strtotime($date);
                $shortdate = date('d-m-Y', $s);
                $nextKm = $km;
                $km = $r['km'];
                $lastvolume = 0 + $volume;
                $volume = $r['volume'];
                $location = $r['location'];
                $type = $r['type'];
                $registration = $r['registration'];


                $range =  $nextKm  - $km;
                if ($range < 0) {
                    $range = 1;
                }
                if ($lastvolume == 0 || $volume == 0) {
                    $consumption = 0;
                } else {
                    $consumption = (100 * $volume) / $range;
                }
                $consumptionView = round($consumption, 2);

                echo '<tr>  
                           <td align="center">' . $shortdate . '</td>
                            <td align="center">' . $km . '</td>
                            <td align="center">' . $volume . '</td>
                            <td align="center">' . $consumptionView . '</td>';

                if ($shortdate == $datetoday) {
         
                    echo '<td align="center"><a href="sys-backend/delmyrefueling.php?id=' . $sysid . '"><img src="sys-config/trash.png" style="width:30px;height:30px;"></a></td>';
                } else {
                    echo '<td align="center"><img src="sys-config/notallowed.png" style="width:40px;height:28px;"></td>';
                }
                echo '</td></tr>';
            }
            echo "  </table></div><br> * $lang_WorkTxt <br>";
        } else {
            echo "<font color= 'black'>$lang_EmptyTable</font> ";
        }
        $conn->close();
        echo "
        </div>
        </div>
        <div id='menu'><a href='index.php'>$lang_main</a>";
    } else {

        echo $nologin;
    }

    ?>

    <script>
        function geoFindMe() {

            const status = document.querySelector('#geostatus');

            function success(position) {
                const latitude = position.coords.latitude;
                const longitude = position.coords.longitude;

                latitude2 = latitude.toFixed(2);
                longitude2 = longitude.toFixed(2);

                document.getElementById("latitude").value = latitude;
                document.getElementById("longitude").value = longitude;
                document.getElementById("location").value = `GPS: ${latitude2} - ${longitude2}`;

                status.textContent = '';
            }

            function error() {
                status.textContent = 'Unable to retrieve your location';
            }

            if (!navigator.geolocation) {
                status.textContent = 'Geolocation is not supported by your browser';
            } else {
                status.textContent = 'Locating…';
                navigator.geolocation.getCurrentPosition(success, error);
            }
        }
        document.querySelector('#find-me').addEventListener('click', geoFindMe);
    </script>


</html>