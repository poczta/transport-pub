<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style2.css" />

    <?php
    include "sys-config/lang.php";
    include "sys-config/config.php";
    include "sys-backend/dailycheck.php";
    include "sys-backend/nologin.php";

    echo '<title>' . $lang_rqc . '</title>
</head>
<body>

    <div id="header">
        <div id="logo">
            <h3>' . $lang_rqc . '</h3> 
        </div>
    </div>
    <center>
    <div id="wrapper">
        <div id="content">';

    session_start();
    if ($_SESSION['user_id'] != null) {
        $zalogowanyID = $_SESSION['user_id'];
        $today = date("Y-m-d H:i:s");
        $s1 = strtotime($today);
        $datetoday =  date('m/d/Y', $s1);

        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }


        $sql = "SELECT * FROM FreightRequest where assigned_id = $zalogowanyID and freight_status >= 3 ORDER BY sysid DESC limit 5";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {

            echo "<div id='table'><h3>$Last_5_entries</h3>
                    <table cellspacing='0' cellpadding='10' >
                        <tr align='center'>

                        <th>$lang_number</th>
                        <th>$lang_status</th>
                        <th>$lang_preview</th>
                        </tr>";

            while ($r = $result->fetch_assoc()) {
                $sysid = $r['sysid'];
                $freight_prefix = $r['freight_prefix'];
                $freight_status = $r['freight_status'];
                $sender_name = $r['sender_name'];
                $sender_postal = $r['sender_postal'];
                $recipient_name = $r['recipient_name'];
                $recipient_postal = $r['recipient_postal'];
                $assigned_id = $r['assigned_id'];


                switch (true) {
                    case ($sysid < 10):
                        $prefixName = $freight_prefix . '000000' . $sysid;
                        break;
                    case ($sysid < 100):
                        $prefixName = $freight_prefix . '00000' . $sysid;
                        break;
                    case ($sysid < 1000):
                        $prefixName = $freight_prefix . '0000' . $sysid;
                        break;
                    case ($sysid < 10000):
                        $prefixName = $freight_prefix . '000' . $sysid;
                        break;
                    case ($sysid < 10000):
                        $prefixName = $freight_prefix . '00' . $sysid;
                        break;
                    case ($sysid < 10000):
                        $prefixName = $freight_prefix . '0' . $sysid;
                        break;
                }

                switch ($freight_status) {
                    case 3:
                        $statename = $lang_state_todo;
                        $statecolor = 'green';
                        break;
                    case 4:
                        $statename = $lang_state_wip;
                        $statecolor = 'orange';
                        break;
                    case 5:
                        $statename = $lang_state_closed;
                        $statecolor = 'grey';
                        break;
                }

                echo '<tr >  <form action="orderpreview.php" method="GET">
                <input type=hidden name="reqid" value="' . $sysid . '" >


                            <td align="center">' . $prefixName . '</td>
                            <td align="center">' . $statename . '</td>
                            <td align="center"><input type="submit" value="&nbsp;&nbsp;' . $lang_preview . '&nbsp;&nbsp;"; style="background-color:' . $statecolor . '"></input></td>
                          </form> </td>';

                echo '</td></tr>';
            }
            echo "  </table></div><br>";
        } else {
            echo "<font color= 'black'>$lang_EmptyTable</font> ";
        }
        $conn->close();
        echo '
        </div>
        </div>
        <div id="menu"><ul>
        <li>  <a href="index.php">' . $lang_main . '</a> </li>
        <li>  <a href="work.php">' . $lang_work . '</a> </li>
        </ul></div>';
    } else {
        echo $nologin;
    }

    ?>
    </div>
    </div>

</html>