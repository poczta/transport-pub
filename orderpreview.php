<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style2.css" />

    <?php
    include "sys-config/lang.php";
    include "sys-config/config.php";
    include "sys-backend/dailycheck.php";
    include "sys-backend/nologin.php";

    echo '<title>' . $lang_rqc . '</title>
</head>
<body>

    <div id="header">
        <div id="logo">
            <h3>' . $lang_rqc . '</h3> 
        </div>
    </div>
    <center>
    <div id="wrapper">
        <div id="content">';

    session_start();
    if ($_SESSION['user_id'] != null) {
        $zalogowanyID = $_SESSION['user_id'];
        $today = date("Y-m-d H:i:s");
        $s1 = strtotime($today);
        $datetoday =  date('m/d/Y', $s1);
        $getid =  ($_GET['reqid']);

        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }


        $sql = "SELECT * FROM FreightRequest where sysid = $getid limit 1";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {

            while ($r = $result->fetch_assoc()) {
                $freight_prefix = $r['freight_prefix'];
                $sysid = $r['sysid'];
                $freight_status = $r['freight_status'];
                $freight_username = $r['freight_username'];
                $create_timestamp = $r['create_timestamp'];

                $recipient_name = $r['recipient_name'];
                $recipient_country = $r['recipient_country'];
                $recipient_city = $r['recipient_city'];
                $recipient_postal = $r['recipient_postal'];
                $recipient_street = $r['recipient_street'];
                $recipient_phone = $r['recipient_phone'];
                $recipient_additional = $r['recipient_additional'];
                $recipient_datetime = date('Y-m-d\TH:i', strtotime($r['recipient_datetime']));

                $sender_name = $r['sender_name'];
                $sender_country = $r['sender_country'];
                $sender_city = $r['sender_city'];
                $sender_postal = $r['sender_postal'];
                $sender_street = $r['sender_street'];
                $sender_phone = $r['sender_phone'];
                $sender_additional = $r['sender_additional'];
                $sender_datetime = date('Y-m-d\TH:i', strtotime($r['sender_datetime']));
                $GetUserName = $r['assigned_name'];
                $GetUserID = $r['assigned_id'];


                echo '<table "><form action="workload.php" method="POST">
                <input type=hidden name="citycode" value="' . $sender_postal . '" ></input>
                <input type=hidden name="country" value="' . $sender_country . '" ></input>
                <input type=hidden name="cityname" value="' . $sender_city . '" ></input>
                <input type=hidden name="reqid" value="' . $sysid . '" ></input>
                <tr><th colspan="2" width="100px">Dane Nadawcy</th></tr><tr></tr>
                <tr><td>Nazwa Nadawcy</td><td>' . $sender_name . '</td></tr><tr></tr>
                <tr><td>Kraj Nadawcy</td><td>' . $sender_country . '</td></tr><tr></tr>
                <tr><td>Kod pocztowy </td><td>' . $sender_postal . '</td></tr><tr></tr>
                <tr><td>Miejscowość</td><td>' . $sender_city . '</td></tr><tr></tr>
                <tr><td>Ulica Nadawcy</td><td>' . $sender_street . '</td></tr><tr></tr>
                <tr><td>Telefon Nadawcy</td><td>' . $sender_phone . '</td></tr><tr></tr>
                <tr><td>Dodatkowe informacje</td><td>' . $sender_additional . '</td></tr><tr></tr>
                <tr><td>Data odbioru</td><td>' . $sender_datetime . '</td></tr>

                <td><div class="form-group" style="text-align: center; float:right"><input type="submit" value="&nbsp; Potwierdz załadunek &nbsp;" style="background-color:orange;color:white; border-radius: 5px"></td></td><td></tr><tr></tr>
                </form>';

                echo '<table "><form action="workunload.php" method="POST">
                <input type=hidden name="citycode" value="' . $recipient_postal . '" ></input>
                <input type=hidden name="country" value="' . $recipient_country . '" ></input>
                <input type=hidden name="cityname" value="' . $recipient_city . '" ></input>
                <input type=hidden name="reqid" value="' . $sysid . '" ></input>
                <tr><th colspan="2" width="100px">Dane odbiorcy</th></tr><tr></tr>
                <tr><td>Nazwa odbiorcy</td><td>' . $recipient_name . '</td></tr><tr></tr>
                <tr><td>Kraj odbiorcy</td><td>' . $recipient_country . '</td></tr><tr></tr>
                <tr><td>Kod pocztowy</td><td>' . $recipient_postal . '</td></tr><tr></tr>
                <tr><td>Miejscowość</td><td>' . $recipient_city . '</td></tr><tr></tr>
                <tr><td>Ulica odbiorcy</td><td>' . $recipient_street . '</td></tr><tr></tr>
                <tr><td>Telefon odbiorcy</td><td>' . $recipient_phone . '</td></tr><tr></tr>
                <tr><td>Dodatkowe informacje</td><td>' . $recipient_additional . '</td></tr><tr></tr>
                <tr><td>Data dostarczenia</td><td>' . $recipient_datetime . '</td></tr>

                <td><div class="form-group" style="text-align: center; float:right"><input type="submit" value="&nbsp; Potwierdź rozładunek &nbsp;" style="background-color:red;color:white; border-radius: 5px"></td><td></td></tr><tr></tr>
                </table>';
            }
        } else {
            echo "<font color= 'black'>$lang_EmptyTable</font> ";
        }
        $conn->close();
        echo '
        </div>
        </div>
        <div id="menu"><ul>
        <li>  <a href="index.php">' . $lang_main . '</a> </li>
        <li>  <a href="orderlist.php">' . $lang_work . '</a> </li>
        </ul></div>';
    } else {

        echo $nologin;
    }
    ?>
            </div>
    </div>

</html>