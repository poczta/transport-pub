<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style2.css" />

    <?php
    include "sys-config/lang.php";
    include "sys-config/config.php";
    include "sys-backend/dailycheck.php";
    include "sys-backend/nologin.php";

    echo '<title>' . $lang_work . '</title>
</head>
<body>

    <div id="header">
        <div id="logo">
            <h3>' . $lang_work . '</h3> 
        </div>
    </div>
    <center>
    <div id="wrapper">
        <div id="content">';

    session_start();
    if ($_SESSION['user_id'] != null) {
        $zalogowanyID = $_SESSION['user_id'];
        $today = date("Y-m-d H:i:s");
        $s1 = strtotime($today);
        $datetoday =  date('m/d/Y', $s1);

        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // 3 = is delegate and comes from sys-backend/dailycheck.php
        if ($delegate == 3 && $ifokdaily == 1) {
            echo '
            <table width="70%" border="0" class="table">
                <tr height="80px" align="center">
                    <td style="background-color:#3399ff"><a href="workload.php" style="display:block; color: white; font-size: 25px; text-decoration: none; text-color: white ">' . $lang_loading . '</a></td>
                    <td style="background-color:#0073e6"><a href="workunload.php" style="display:block; color: white;font-size: 25px; text-decoration: none; text-color: white ">' . $lang_unloading . '</a></td>
                    </tr><tr height="80px" align="center"><td colspan = 2 style="background-color:#00ace6"><a href="orderlist.php" style="display:block; color: white;font-size: 25px; text-decoration: none; text-color: white ">' . $lang_rqc . ' ('.$ReqCount.')</a></td>';
                
                    echo ' </tr>
                </tr>
            </Table>';
        } elseif ($delegate != 3 && $ifokdaily == 1) {
            echo '
            <table width="70%" border="0" class="table">
                <tr height="80px" align="center">
                    <td style="background-color:#737373"><a href="#" style="display:block; color: white; font-size: 25px; text-decoration: none; text-color: white ">' . $lang_loading . '</a></td>
                    <td style="background-color:#404040"><a href="#" style="display:block; color: white;font-size: 25px; text-decoration: none; text-color: white ">' . $lang_unloading . '</a></td>
                    </tr><tr height="80px" align="center"><td colspan = 2 style="background-color:#555555"><a href="#" style="display:block; color: white;font-size: 25px; text-decoration: none; text-color: white ">' . $lang_rqc . ' ('.$ReqCount.')</a></td>
                    </tr>
                </tr>
              
            </Table>';
            echo '<h2><font color=red>' . $lang_noDelegation . ' </font></h2>';
        } else {
            echo '
            <table width="70%" border="0" class="table">
                <tr height="80px" align="center">
                    <td style="background-color:#737373"><a href="#" style="display:block; color: white; font-size: 25px; text-decoration: none; text-color: white ">' . $lang_loading . '</a></td>
                    <td style="background-color:#404040"><a href="#" style="display:block; color: white;font-size: 25px; text-decoration: none; text-color: white ">' . $lang_unloading . '</a></td>
                    </tr><tr height="80px" align="center"><td colspan = 2 style="background-color:#555555"><a href="#" style="display:block; color: white;font-size: 25px; text-decoration: none; text-color: white ">' . $lang_rqc . ' ('.$ReqCount.')</a></td>
                    </tr>
                </tr>
              
            </Table>';
            echo '<h2><font color=red>' . $lang_noDaily . ' </font></h2>';
        }



        $sql = "SELECT * FROM WorkFact where userid = $zalogowanyID ORDER BY sysid DESC limit 5";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {

            echo "<div id='table'><h3>$Last_5_entries</h3>
                    <table cellspacing='0' cellpadding='10' >
                        <tr align='center'>

                        <th>$lang_date</th>
                        <th>$lang_type</th>
                        <th>$lang_country</th>
                        <th>$lang_cityCode</th>
                        <th>$lang_doc</th>
                        <th>$lang_action</th>

                        </tr>";

            while ($r = $result->fetch_assoc()) {
                $sysid = $r['sysid'];
                $date = $r['date'];
                $s = strtotime($date);
                $shortdate = date('m/d/Y', $s);
                $shortdateview = date('d/m/Y H:i:s', $s);
                $nextKm = $km;
                $km = $r['km'];
                $country = $r['country'];
                $type = $r['type'];
                $citycode = $r['citycode'];
                $registration = $r['registration'];
                $freightID = $r['freightID'];

                switch ($type) {
                    case 1:
                        $typeView = '<img src="sys-config/load.png" style="width:50px;height:52px;">';
                        break;
                    case 2:
                        $typeView = '<img src="sys-config/home.png" style="width:40px;height:40px;">';
                        break;
                    case 3:
                        $typeView = '<img src="sys-config/wheel.png" style="width:40px;height:40px;">';
                        break;
                    case 4:
                        $typeView = '<img src="sys-config/unload.png" style="width:40px;height:35px;">';
                        break;
                }

                echo '<tr>  
                            <td align="center">' . $shortdateview . '</td>
                            <td align="center" >' . $typeView . '</td>
                            <td align="center">' . $country . '</td>
                            <td align="center">' . $citycode . '</td>';
                            if ($type == 4) {echo '<td align="center"><a href="attachments.php?id=' . $freightID . '">' . $lang_preview . '</a></td>';}
                            else {echo '<td></td>';}

                if ($shortdate == $datetoday) {
                    echo '<td align="center"><a href="sys-backend/delmywork.php?id=' . $sysid . '"><img src="sys-config/trash.png" style="width:30px;height:30px;"></a></td>';
                } else {
                    echo '<td align="center"><img src="sys-config/notallowed.png" style="width:40px;height:28px;"></td>';
                }
                echo '</td></tr>';
            }
            echo "  </table></div><br> * $lang_WorkTxt <br>";
        } else {
            echo "<font color= 'black'>$lang_EmptyTable</font> ";
        }
        $conn->close();
        echo "
        </div>
        </div>
        <div id='menu'><a href='index.php'>$lang_main</a>";
    } else {

        echo $nologin;
    }
    
    ?>
    </center></body>
</html>