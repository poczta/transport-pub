<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" W>
<html xmlns="http://www.w3.org/1999/xhtml">
<html translate="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style2.css" />

    <?php
    include "sys-config/lang.php";
    include "sys-config/config.php";
    include "sys-backend/nologin.php";

    echo '<title>' . $lang_unloading . '</title>
</head>
<body>
    <div id="header">
        <div id="logo">
            <h3>' . $lang_unloading . '</h3> 
        </div>
    </div>   
     <center>
    <div id="wrapper">
        <div id="content">';



    session_start();
    if ($_SESSION['user_id'] != null) {
        $zalogowanyID = $_SESSION['user_id'];
        $today = date("Y-m-d H:i:s");
        $s1 = strtotime($today);
        $datetoday =  date('m/d/Y', $s1);
        $getcountry =  ($_POST['country']);
        $getcityname =  ($_POST['cityname']);
        $getreqid =  ($_POST['reqid']);


        echo '<table style="width:80%"><tr>
        <form action="workunload.php" method="POST">
        <td> <label> ' . $lang_cityCode . ' </label></td><td><input type=text id="citycode" name="citycode" size="20"></input></td>';

        echo '</td></tr><tr><td></td><td>';
        echo '<div class="form-group" style="text-align: center; float:right">';
        echo '<input type="submit" value="' . $lang_submit . '"></div>';
        echo '</td></tr>';
        echo ' </form>';




        $conn = new mysqli($servername, $username, $password, $dbname);
        if (!$conn) {
            die('Sorry, Dear Please check your details, or Could not Connect MySql Server:');
        }

        //=================== get city from postal //===================
        if (isset($_POST['citycode'])) {
            $codeforcheckin =  ($_POST['citycode']);

            $query = "SELECT * FROM postal WHERE postalcode = '$codeforcheckin' LIMIT 50";
            $result = mysqli_query($conn, $query);
            if (mysqli_num_rows($result) > 0) {
                while ($r = mysqli_fetch_array($result)) {
                    $cityname = $r['cityname'];
                    $postnr = $r['postalcode'];
                    $country = $r['country'];
                }
                echo '</tr><tr><td>' . $lang_City . ' </td><td></td></tr>';
            } else {
                echo '</tr><tr><th colspan="2">' . $lang_NoCity . ' </th></tr>';
            }

            //=================== get city from postal end//===================
            echo        '<tr><td colspan="2"><hr></td></tr><tr></tr>';
            echo        '<form action="sys-backend/addwork.php" method="POST">';
            echo        '<input type=hidden name="loadtype" value="4" ></input>';
            echo        '<input type=hidden name="reqid" value="' . $getreqid . '" ></input>';
            echo        '<tr><td><label>' . $lang_cityCode . '</label></td><td><input type=text name="citycode" size="20" value=' . $postnr . ' ></input></td></tr>';


            if ($country == null) {
                echo '<tr></tr><tr><td><label>' . $lang_country . '</label></td><td><input type=text name="country" size="20" value=' . $country . '></input></td></tr>';
            } else {
                echo '<tr></tr><tr><td><label>' . $lang_country . '</label></td><td><select id="country" name="country">';

                $query = "SELECT distinct country FROM postal WHERE postalcode = '$codeforcheckin' LIMIT 10";
                $result = mysqli_query($conn, $query);
                if (mysqli_num_rows($result) > 0) {
                    while ($r = mysqli_fetch_array($result)) {
                        $country = $r['country'];
                        if ($getcountry == $country) {
                            $countryselected = "selected";
                        } else {
                            $countryselected = "";
                        }
                        echo            '<option value="' . $country . '"' . $countryselected . '>' . $country . '</option>';
                    }
                }
                echo '</select></td></tr>';
            }



            if ($cityname == null) {
                echo '<tr></tr><tr><td><label>' . $lang_city . '</label></td><td><input type=text name="cityname" size="20" value=' . $cityname . ' ></input></td></tr>';
            } else {
                echo '<tr></tr><tr><td><label>' . $lang_city . '</label></td><td><select id="cityname" name="cityname">';

                $query = "SELECT * FROM postal WHERE postalcode = '$codeforcheckin' LIMIT 50";
                $result = mysqli_query($conn, $query);
                if (mysqli_num_rows($result) > 0) {
                    while ($r = mysqli_fetch_array($result)) {
                        $cityname = $r['cityname'];
                        if ($cityname == $getcityname) {
                            $citynameselected = "selected";
                        } else {
                            $citynameselected = "";
                        }
                        echo            '<option value="' . $cityname . '"' . $citynameselected . '>' . $cityname . '</option>';
                    }
                }
                echo '</select></td></tr>';
            }

            //------ tu weryfikacja stanu licznika czy nie jest mniejszy lub wiekszy niz 1000km 
            $sql2 = "SELECT * FROM WorkFact where userid = $zalogowanyID ORDER BY sysid DESC LIMIT 1";
            $result2 = $conn->query($sql2);
            if ($result2->num_rows > 0) {
                while ($r2 = $result2->fetch_assoc()) {
                    $checkkmmin = $r2['km'];
                    $getfreightID = $r2['freightID'];
                }
            }

            $sql1 = "SELECT * FROM devices where assignedid = '$zalogowanyID' LIMIT 1";
            $result1 = $conn->query($sql1);
            if ($result1->num_rows > 0) {
                while ($r1 = $result1->fetch_assoc()) {
                    $devicename = $r1['name'];
                    echo '<tr></tr><tr><td><label>' . $lang_registration . ' </label></td><td><input type=text name="registration" size="20" value=' . $devicename . ' readonly></input></td></tr>';
                }
            }
            echo '<tr></tr><tr><td><label>' . $lang_odometer . '</label></td><td><input type=number name="km" min="' . $checkkmmin . '"  ></input></td>';

            // localisation 
            echo '<tr></tr>';
            echo '<input type=hidden name="latitude"  id="latitude" value=""></input>';
            echo '<input type=hidden name="longitude"  id="longitude"value=""></input>';
            echo '<td><label for="location">' . $lang_location . ' </label></td><td><input type=text name="location"  id="location" disabled></input>';
            echo '<button  type="button" id = "find-me">' . $lang_get_locations . '</button><br/><p id = "geostatus">';
            echo '<tr></tr><tr><td><label>' . $lang_canceled . '</label></td><td><input type=checkbox name="canceled" id="canceled" ></input></td></tr></td></tr>';
            echo '</td></tr><tr><td></td><td>';
            echo '<div class="form-group" style="text-align: center; float:right">';
            echo '<input type="submit" value="' . $lang_submit . '"></div>';
            echo '</td></tr></table> <br>';
        } else      echo '</form></td></tr></table> <br>';


        $conn->close();
        echo "
        </div>
        </div>
        <div id='menu'><a href='index.php'>$lang_main</a>";
    } else {

        echo $nologin;
    }

    if (isset($_GET["redirect"])) {
        header("location: work.php");
    }

    ?>
    <script>
        function geoFindMe() {

            const status = document.querySelector('#geostatus');

            function success(position) {
                const latitude = position.coords.latitude;
                const longitude = position.coords.longitude;

                latitude2 = latitude.toFixed(2);
                longitude2 = longitude.toFixed(2);

                document.getElementById("latitude").value = latitude;
                document.getElementById("longitude").value = longitude;
                document.getElementById("location").value = `GPS: ${latitude2} - ${longitude2}`;

                status.textContent = '';
            }

            function error() {
                status.textContent = 'Unable to retrieve your location';
            }

            if (!navigator.geolocation) {
                status.textContent = 'Geolocation is not supported by your browser';
            } else {
                status.textContent = 'Locating…';
                navigator.geolocation.getCurrentPosition(success, error);
            }
        }
        document.querySelector('#find-me').addEventListener('click', geoFindMe);
    </script>
    </ul>
    </div>
    </div>
    </center>
    </body>

</html>